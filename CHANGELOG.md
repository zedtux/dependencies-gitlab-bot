# ChangeLog

## Added
 - Implements cloning project, search for Gemfile
 - Implements Gemfile gems updates
 - Implements creating a merge request per gem
 - Implements merge conflict detection and branch rebase
