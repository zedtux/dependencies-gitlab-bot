# Dependencies management bot for Gitlab

The aim of this project is to create merge requests to update given projects
dependencies, using the Gitlab pipelines and API.

I was using [Dependabot](https://dependabot.com) on Github before, since I
migrated to GitLab after Microsoft bought Github.com, I didn't find any existing
solution.

I have asked Dependabot for a GitLab version, they told me they will do it,
but no news since this, so I decided to build my own.

## Why this project uses the Gitlab pipelines?

I didn't want to pay a server and have to maintain it, so I looked for a feature
in GitLab to run my project.

Thanks to Docker and the GitLab Pipeline Schedules, this is possible and for
free.

Of course, there is no strong dependency to GilLab. Actually it's just Docker.
You can run it on your machine, that will be the same result.
Update the `docker-compose.yml` file for you and use `docker-compose` to run it
on your machine, or your server if you prefer 😉.

## Usage

### Use zedtux's instance

Edit the [Dependencies Gitlab Bot fork](https://gitlab.com/dependencies-bot/dependencies-gitlab-bot/edit/master/projects.yml)'s
 `projects.yml` file and add your Gitlab project ID.

### Running your own instance

1. Fork this project
2. Update the `variables` from the `.gitlab-ci.yml` file
3. Update the `projects.yml` file with your projects
4. Create the `SSH_PRIVATE_KEY` and `GITLAB_PRIVATE_TOKEN` variables from
Settins > CI / CD > Variables
5. Setup the a Pipeline Schedule in order to run this project

## TODO

 - [ ] Improve own instance documentation
