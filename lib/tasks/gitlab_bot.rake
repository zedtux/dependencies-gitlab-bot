require_relative '../dependencies/gitlab_bot'

namespace :gitlab_bot do
  desc 'Fork projects, and create a branches updating gems'
  task :dependencies do
    context = OrganizeWritingSshKeys.call
    raise StandardError, context.error if context.failure?

    context = OrganizePostingMergeRequestsToUpdateProjectsGems.call
    raise StandardError, context.error if context.failure?
  end
end
