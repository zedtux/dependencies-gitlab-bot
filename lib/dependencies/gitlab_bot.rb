require 'interactor'

require 'dependencies/gitlab_bot/version'

require 'interactors/organize_posting_merge_requests_to_update_projects_gems'
require 'interactors/organize_writing_ssh_keys'

module Dependencies
  module GitlabBot
  end
end
