module YamlHelpers
  def self.project_yml_file_path
    File.join(ENV['ROOT_PATH'], 'projects.yml')
  end
end
