module Bundler
  class EnsureAGemfileExists
    include Interactor

    def call
      sanity_checks!

      context.local_project_gemfile_path = build_gemfile_path

      puts "[#{self.class.name}] Checking file " \
           "#{context.local_project_gemfile_path} exists ..."
      return if File.exist?(context.local_project_gemfile_path)
      context.fail!(error: 'Project has no Gemfile')
    end

    private

    def sanity_checks!
      return if context.local_git_repo_path

      context.fail!(error: 'Local git repo path is not defined.')
    end

    def build_gemfile_path
      File.join(context.local_git_repo_path, 'Gemfile')
    end
  end
end
