require 'bundler/injector'

module Bundler
  class UpdateGemfileLock
    include Interactor

    def call
      sanity_checks!

      puts "[#{self.class.name}] Building Bundler definition ..."

      builder = Bundler::Dsl.new
      builder.eval_gemfile(context.local_project_gemfile_path)

      definition = builder.to_definition(context.project_gemfilelock_path,
                                         gems: [context.gem_name])

      puts "[#{self.class.name}] Resolving dependencies remotely ..."
      # Update the definition with new requirements from updated gem
      definition.resolve_remotely!

      puts "[#{self.class.name}] Locking #{context.project_gemfilelock_path} ..."
      # Write the update to the Gemfile.lock
      definition.lock(context.project_gemfilelock_path)
    end

    private

    def sanity_checks!
      unless context.gem_name
        context.fail!(error: { gem_name: 'is missing' })
      end

      unless context.local_project_gemfile_path
        context.fail!(error: { local_project_gemfile_path: 'is missing' })
      end

      return if context.project_gemfilelock_path

      context.fail!(error: { project_gemfilelock_path: 'is missing' })
    end
  end
end
