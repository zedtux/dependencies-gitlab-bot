require 'bundler/shared_helpers'

module Bundler
  class UpdateGemfile
    include Interactor

    def call
      sanity_checks!

      new_gemfile = IO.readlines(context.local_project_gemfile_path)
      new_gemfile = update_gemfile(new_gemfile)
      write_gemfile(new_gemfile)

      context.gemfile_updated = true
    end

    private

    def sanity_checks!
      unless context.gem_name
        context.fail!(error: { gem_name: 'is missing' })
      end

      unless context.next_version
        context.fail!(error: { next_version: 'is missing' })
      end

      return if context.local_project_gemfile_path

      context.fail!(error: { local_project_gemfile_path: 'is missing' })
    end

    def build_regex_detect_gem
      /gem\s+['"]#{context.gem_name}['"]/
    end

    def build_regex_extract_version
      /gem\s*['"]#{context.gem_name}['"]\s*,\s*['"](?:~>|>=|=)?\s*([\d\.\-A-z]+)['"]/
    end

    def build_regex_replace_version
      /(['"])([~>=]+?)\s*([\d\.\-A-z]+)(['"])/
    end

    def extract_version_from(line)
      line.scan(build_regex_extract_version).flatten
    end

    def update_gem_version(line)
      line.sub(build_regex_replace_version) do
        "#{$1}#{$2} #{context.next_version}#{$4}"
      end
    end

    def update_gemfile(gemfile)
      # Using `each_with_index` over `.each.with_index`
      # https://stackoverflow.com/a/20258604/1996540
      gemfile.each_with_index do |line, index|
        # Search the line for the current gem
        if gemfile[index] =~ build_regex_detect_gem
          version = extract_version_from(gemfile[index]).first
          puts "[#{self.class.name}] Gem found with version #{version}. " \
               'Updating ...'
          before = gemfile[index]
          gemfile[index] = update_gem_version(gemfile[index])
          puts "[#{self.class.name}] Updated #{before.inspect} to " \
               "#{gemfile[index].inspect}"
        end
      end

      gemfile.join.chomp
    end

    def write_gemfile(gemfile)
      gemfile_path = context.local_project_gemfile_path

      # TODO : Replace with `Bundler::SharedHelpers.write_to_gemfile` when
      # available.
      Bundler::SharedHelpers.filesystem_access(gemfile_path) do |g|
        File.open(g, "w") { |file| file.puts gemfile }
      end
    end
  end
end
