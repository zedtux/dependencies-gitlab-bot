require 'bundler'

module Bundler
  class FetchGemListFromProjectGemfile
    include Interactor

    def call
      sanity_checks!

      puts "[#{self.class.name}] Loading project's gem list from " \
           "#{context.local_project_gemfile_path} ..."
      builder = Bundler::Dsl.new
      context.project_dependencies = builder.eval_gemfile(gemfile_path)

      build_gemfile_lock_path && build_bundler_definition(builder)
    end

    private

    def sanity_checks!
      return if context.local_git_repo_path

      context.fail!(error: 'Local git repo path is not defined.')
    end

    def build_bundler_definition(builder)
      context.bundler_definition = builder.to_definition(
        context.project_gemfilelock_path,
        {}
      )
    end

    def build_gemfile_lock_path
      context.project_gemfilelock_path = context.local_project_gemfile_path.dup
      context.project_gemfilelock_path << '.lock'
    end

    def gemfile_path
      context.local_project_gemfile_path
    end
  end
end
