require 'git'

module Git
  class CommitAndPush
    include Interactor

    def call
      sanity_checks!

      commit && push
    rescue Git::GitExecuteError => error
      context.fail!(error: 'Something prevented from commiting and pushing ' \
                           "from the git repository: #{error.message}")
    end

    private

    def sanity_checks!
      unless context.gem_name
        context.fail!(error: { gem_name: 'is missing' })
      end

      unless context.current_version
        context.fail!(error: { current_version: 'is missing' })
      end

      unless context.next_version
        context.fail!(error: { next_version: 'is missing' })
      end

      unless context.local_project_gemfile_path
        context.fail!(error: { local_project_gemfile_path: 'is missing' })
      end

      unless context.git_branch
        context.fail!(error: { git_branch: 'is missing' })
      end

      return if context.local_git_repo

      context.fail!(error: 'Missing reference to local git repo.')
    end

    def commit
      # Gemfile and Gemfile.lock
      puts "[#{self.class.name}] Git adding " \
           "#{context.local_project_gemfile_path + '*'} ..."
      context.local_git_repo.add(context.local_project_gemfile_path + '*')

      puts "[#{self.class.name}] Commit with message #{commit_message} ..."
      commit = context.local_git_repo.commit(commit_message)
      puts "[#{self.class.name}] commit: #{commit.inspect}"

      true
    end

    def commit_message
      "Bump #{context.gem_name} from #{context.current_version} to " \
      "#{context.next_version}"
    end

    def push
      puts "[#{self.class.name}] Pushing branch #{context.git_branch.name} ..."
      context.local_git_repo.push('origin', context.git_branch.name)
    end
  end
end
