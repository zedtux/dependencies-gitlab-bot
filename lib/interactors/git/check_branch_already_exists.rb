module Git
  class CheckBranchAlreadyExists
    include Interactor

    def call
      sanity_checks!

      context.git_branch_name = build_git_branch_name
      context.branch_exists = do_a_branch_already_exists?

      return unless context.branch_exists

      context.git_branch_sha = fetch_last_branch_commit_sha
    rescue Git::GitExecuteError => error
      context.fail!(error: 'Something prevented from checking the git branch' \
                           ": #{error.message}")
    end

    private

    def sanity_checks!
      unless context.gem_name
        context.fail!(error: { gem_name: 'is missing' })
      end

      unless context.next_version
        context.fail!(error: { next_version: 'is missing' })
      end

      unless context.local_project
        context.fail!(error: { local_project: 'is missing' })
      end

      return if context.local_git_repo

      context.fail!(error: 'Missing reference to local git repo.')
    end

    def build_git_branch_name
      @branche_name ||= [
        Dependencies::GitlabBot::Helpers.namespace,
        context.gem_name,
        context.next_version
      ].join('/')
    end

    def do_a_branch_already_exists?
      context.local_git_repo.branches.remote.detect do |branch|
        branch.name == context.git_branch_name
      end != nil
    end

    def fetch_last_branch_commit_sha
      remote_commit = Git.ls_remote(
        context.local_project.http_url_to_repo,
        context.git_branch_name
      )

      return nil unless remote_commit

      remote_commit['branches'][context.git_branch_name][:sha]
    end
  end
end
