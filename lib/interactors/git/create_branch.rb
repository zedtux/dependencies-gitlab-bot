require 'git'

module Git
  #
  # Create the Git branch which will be used to push the gem update if there is
  # no git branch.
  # When a branch already exists, triggers the merge request maintenance
  # organizer.
  #
  class CreateBranch
    include Interactor

    def call
      sanity_checks!

      context.branch_name = build_git_branch_name

      puts "[#{self.class.name}] No exiting branch, creating a new one ..."
      checkout_master && create_branch
    rescue Git::GitExecuteError => error
      context.fail!(error: 'Something prevented from creating the git branch ' \
                           "in the git repository: #{error.message}")
    end

    private

    def sanity_checks!
      unless context.gem_name
        context.fail!(error: { gem_name: 'is missing' })
      end

      unless context.next_version
        context.fail!(error: { next_version: 'is missing' })
      end

      return if context.local_git_repo

      context.fail!(error: 'Missing reference to local git repo.')
    end

    def build_git_branch_name
      @branche_name ||= [
        Dependencies::GitlabBot::Helpers.namespace,
        context.gem_name,
        context.next_version
      ].join('/')
    end

    def checkout_branch
      puts "[#{self.class.name}] Checking out the #{context.branch_name} " \
           'branch ...'
      context.local_git_repo.branch(context.branch_name).checkout
      update_git_branch
    end

    def checkout_master
      puts "[#{self.class.name}] Checking out the master branch ..."
      context.local_git_repo.branch('master').checkout
    end

    def create_branch
      puts "[#{self.class.name}] Creating a new Git branch with name " \
           "#{context.branch_name} ..."

      checkout_branch
    rescue Git::GitExecuteError => error
      puts "[#{self.class.name}]: ERROR while trying to create a new branch: " \
           "#{error.inspect}"
      context.fail!(error: "Could not create Git branch #{branch_name}")
    end

    def do_a_branch_already_exists?
      context.local_git_repo.branches.remote.detect do |branch|
        branch.name == context.branch_name
      end != nil
    end

    def update_git_branch
      context.git_branch = context.local_git_repo.branch(context.branch_name)
    end
  end
end
