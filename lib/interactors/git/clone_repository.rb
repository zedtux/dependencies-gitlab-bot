require 'git'
require 'logger'

module Git
  class CloneRepository
    include Interactor

    def call
      sanity_checks!

      clone_remote_project_locally
      configure_local_git_repo
    rescue Git::GitExecuteError => error
      context.fail!(error: 'Something prevented from cloning the git ' \
                           "repository: #{error.message}")
    end

    private

    def sanity_checks!
      return if context.local_project

      context.fail!(error: 'No defined local project to clone.')
    end

    def clone_remote_project_locally
      puts "[#{self.class.name}] Cloning #{repo_url} ..."
      project_name = "project-#{project_id}"
      context.local_git_repo_path = File.join(ENV['CLONE_PATH'], project_name)
      context.local_git_repo = Git.clone(repo_url, project_name,
                                         path: ENV['CLONE_PATH'],
                                         log: Logger.new(STDOUT))
    end

    def configure_local_git_repo
      context.local_git_repo.config('user.name', ENV['BOT_NAME'])
      context.local_git_repo.config('user.email', ENV['BOT_EMAIL'])
    end

    def repo_url
      context.local_project.ssh_url_to_repo
    end

    def project_id
      context.remote_project.id
    end
  end
end
