module Git
  class InitializeClient
    include Interactor

    def call
      Git.configure do |config|
        config.git_ssh = File.join(ENV['ROOT_PATH'], 'bin', 'ssh_wrapper')
      end
    end
  end
end
