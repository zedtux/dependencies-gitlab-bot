require_relative 'git/add_upstream_when_missing'
require_relative 'git/checkout_branch'
require_relative 'git/checkout_master_branch_and_pull'
require_relative 'git/fetch_upstream'
require_relative 'git/rebase_branch'
require_relative 'git/sync_master_branch_with_upstream'
require_relative 'gitlab/check_merge_request_status'
require_relative 'gitlab/find_merge_request'

class OrganizeMergeRequestMaintenance
  include Interactor::Organizer

  organize Gitlab::FindMergeRequest,
           Gitlab::CheckMergeRequestStatus,
           Git::AddUpstreamWhenMissing,
           Git::FetchUpstream,
           Git::SyncMasterBranchWithUpstream,
           Git::CheckoutBranch,
           Git::CheckoutMasterBranchAndPull,
           Git::RebaseBranch
end
