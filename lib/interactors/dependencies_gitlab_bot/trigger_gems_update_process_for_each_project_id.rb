require 'interactors/organize_updating_project_gems'

module DependenciesGitlabBot
  class TriggerGemsUpdateProcessForEachProjectId
    include Interactor

    def call
      sanity_checks!

      context.project_ids.map do |project_id|
        puts "[#{self.class.name}] Processing project ID #{project_id} ..."
        project_update_context = OrganizeUpdatingProjectGems.call(id: project_id)
        if project_update_context.failure?
          context.fail!(error: project_update_context.error)
        else
          puts "[#{self.class.name}] SUCCESS Project ID #{project_id} " \
               'successfully proceeded.'
        end
      end
    end

    private

    def sanity_checks!
      context.fail!(error: 'Missing project IDs') if context.project_ids.nil?

      unless context.project_ids.is_a?(Array)
        context.fail!(error: 'Expected project IDs to be an Array but ' \
                             "is a #{context.project_ids.class}")
      end

      return unless context.project_ids.size.zero?

      context.fail!(error: 'Project IDs is an empty Array')
    end
  end
end
