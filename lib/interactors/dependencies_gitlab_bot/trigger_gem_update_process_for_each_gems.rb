require 'dependencies/gitlab_bot/helpers'
require 'interactors/organize_merge_request_maintenance'
require 'interactors/organize_updating_project_gem'
require 'interactors/git/check_branch_already_exists'

module DependenciesGitlabBot
  #
  # This interactor is responsible to execute the organizer which branch,
  # update a gem, push and create merge request.
  #
  class TriggerGemUpdateProcessForEachGems
    include Interactor

    def call
      sanity_checks!

      project_id = context.id
      remote_project = context.remote_project

      if context.dependencies_hash.keys.count.zero?
        puts "[#{self.class.name}] Project #{remote_project.name} with ID " \
             "#{project_id} seems to have all its dependencies up-to-date 🎉."
        return
      end

      builder = context.builder
      gitlab = context.gitlab
      local_git_repo = context.local_git_repo
      local_project = context.local_project
      local_project_gemfile_path = context.local_project_gemfile_path
      project_gemfilelock_path = context.project_gemfilelock_path

      context.dependencies_hash.each do |gem_name, attributes|
        version = build_next_version(attributes)
        update_kind = build_update_kind(attributes)

        puts "[#{self.class.name}] Processing #{gem_name} gem #{update_kind} " \
             "update from #{attributes[:current_version]} to #{version} for " \
             "project ID #{project_id} ..."

        git_context = Git::CheckBranchAlreadyExists.call(
          gem_name: gem_name,
          local_git_repo: local_git_repo,
          next_version: version,
          local_project: local_project
        )

        git_branch_name = git_context.git_branch_name
        git_branch_sha = git_context.git_branch_sha

        context = if git_context.branch_exists
                    puts "[#{self.class.name}] The branch #{git_branch_name} " \
                         "to update the #{gem_name} gem already exists, " \
                         "let's check if any maintennace is required ..."
                    OrganizeMergeRequestMaintenance.call(
                      current_version: attributes[:current_version],
                      gem_name: gem_name,
                      git_branch_name: git_branch_name,
                      git_branch_sha: git_branch_sha,
                      gitlab: gitlab,
                      local_git_repo: local_git_repo,
                      next_version: version,
                      project_id: project_id,
                      remote_project: remote_project
                    )
                  else
                    puts "[#{self.class.name}] No branch #{git_branch_name} " \
                         "found, let's submit a merge request to upadate the " \
                         "#{gem_name} gem ..."
                    OrganizeUpdatingProjectGem.call(
                      builder: builder,
                      current_version: attributes[:current_version],
                      gem_name: gem_name,
                      git_branch_name: git_branch_name,
                      gitlab: gitlab,
                      local_git_repo: local_git_repo,
                      local_project: local_project,
                      local_project_gemfile_path: local_project_gemfile_path,
                      next_version: version,
                      project_gemfilelock_path: project_gemfilelock_path,
                      project_id: project_id,
                      update_kind: update_kind
                    )
                  end

        if context.failure?
          puts "[#{self.class.name}] ERROR #{context.error}"
        else
          puts "[#{self.class.name}] SUCCESS Project ID #{project_id} " \
               'successfully proceeded.'
        end
      end
    end

    private

    def sanity_checks!
      # Gitlab project ID for which we want to open merge requests
      unless context.id
        context.fail!(error: { id: 'is missing' })
      end

      unless context.dependencies_hash
        context.fail!(error: { dependencies_hash: 'is missing' })
      end

      unless context.dependencies_hash.is_a?(Hash)
        context.fail!(error: { dependencies_hash: 'expected to be a Hash' })
      end

      return if context.gitlab

      context.fail!(error: 'Gitlab client not initialized')
    end

    def build_next_version(attributes)
      Dependencies::GitlabBot::Helpers.build_next_version(attributes)
    end

    def build_update_kind(attributes)
      Dependencies::GitlabBot::Helpers.build_update_kind(attributes)
    end
  end
end
