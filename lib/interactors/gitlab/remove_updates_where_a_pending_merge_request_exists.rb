module Gitlab
  #
  # This interactor is responsible to check all the existing merge requests for
  # the current remote project, and remove from the `context.dependencies_hash`
  # Hash the gems where a pending MR exists, so that at the end, only the gems
  # where a merge request could be created remain in `context.dependencies_hash`
  # Hash.
  #
  class RemoveUpdatesWhereAPendingMergeRequestExists
    include Interactor

    def call
      sanity_checks!

      if context.dependencies_hash.keys.count.zero?
        puts "[#{self.class.name}] Project #{context.remote_project.name} " \
             "with ID #{context.id} seems to have all its dependencies " \
             'up-to-date 🎉.'
        return
      end

      context.dependencies_hash.dup.each do |gem_name, attributes|
        pending_merge_request = find_pending_merge_request_for(gem_name, attributes)

        next unless pending_merge_request

        puts "[#{self.class.name}] Removing #{gem_name} from updates ..."
        context.dependencies_hash.delete(gem_name)
      end
    end

    private

    def sanity_checks!
      # Gitlab project ID for which we want to open merge requests
      unless context.id
        context.fail!(error: { id: 'is missing' })
      end

      unless context.dependencies_hash
        context.fail!(error: { dependencies_hash: 'is missing' })
      end

      unless context.dependencies_hash.is_a?(Hash)
        context.fail!(error: { dependencies_hash: 'expected to be a Hash' })
      end

      return if context.gitlab

      context.fail!(error: 'Gitlab client not initialized')
    end

    def find_pending_merge_request_for(gem_name, attributes)
      project_merge_requests.detect do |merge_request|
        merge_request.name == merge_request_title(gem_name, attributes)
      end
    end

    def merge_request_title(gem_name, attributes)
      Dependencies::GitlabBot::Helpers.build_merge_request_title_for(
        gem_name,
        attributes
      )
    end

    def project_merge_requests
      @merge_requests ||= context.gitlab.merge_requests(context.id)
    end
  end
end
