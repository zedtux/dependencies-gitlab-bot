require_relative 'dependencies_gitlab_bot/create_ssh_folder_if_missing'
require_relative 'dependencies_gitlab_bot/ensure_ssh_keys_environnment_variables_are_defined'
require_relative 'dependencies_gitlab_bot/write_ssh_private_key'

class OrganizeWritingSshKeys
  include Interactor::Organizer

  organize DependenciesGitlabBot::EnsureSshKeysEnvironnmentVariablesAreDefined,
           DependenciesGitlabBot::CreateSshFolderIfMissing,
           DependenciesGitlabBot::WriteSshPrivateKey
end
