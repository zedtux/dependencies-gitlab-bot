require 'gems'

module Rubygems
  #
  # This interactor is responsible to go through the Gemfile definitions and for
  # each gems, look at Rubygems.org stored versions of the that gem.
  #
  # Then, in the case a newer version of the gem, matching your requirements, is
  # available, a GitLab merge request will be created for that.
  # I.e: Your requirement is '~> 1.4.0', your current version is '1.4.0' and a
  #      version 1.4.2 is available, but also a version 1.5.0.
  #      In this run, we will open a MR for version 1.4.2 (soft update). Next
  #      run we go through the next part, continue reading please.
  #
  # In the case a newer version of the gem, NOT matching your requirements, is
  # available, a GitLab merge request will be opened for the newest version,
  # updating your requirements. This is then a proposal, you could reject it.
  # I.e: Your requirement is '~> 1.4.0', your current version is '1.4.2' and
  #      there is no more new versions in the 1.4.x branch available, but a
  #      version 1.5.0 is available.
  #      In this run, we will open a MR for version 1.5.0 (hard update),
  #      which will propose to change '~> 1.4.0' into '~> 1.5.0'.
  #
  # This interactor fills in the `context.dependencies_hash` Hash like the
  # following:
  #
  # {
  #   <gem name> => {
  #     current_version: <spec version>,
  #     updates: {
  #       soft: <Gem::Version of the next version matching spec requirement>,
  #       hard: <Gem::Version of the next version not matching spec requirement>
  #     }
  #   }
  # }
  #
  # Example:
  #
  # {
  #   "selenium-webdriver" => {
  #     current_version: #<Gem::Version "3.13.0">,
  #     updates: {
  #       soft: #<Gem::Version "3.13.1">,
  #       hard: #<Gem::Version "3.14.0">
  #     }
  #   }
  # }
  #
  # With this example, we will open a PR for version 3.13.1. On the next run,
  # so the day after, the `soft` key will no exists, and only the hard one will
  # be here, so we will open a PR for the version 3.14.0 if the previous one has
  # been accepted.
  #
  class SearchDependenciesUpdate
    include Interactor

    def call
      sanity_checks!

      initialize_client

      context.dependencies_hash = {}

      # A project dependency is what you've defined in the Gemfile, with the
      # requirement like ~> 1.0.0, or >= 2.0 and so on.
      context.project_dependencies.each do |dependency|
        puts "[#{self.class.name}][#{dependency.name}] Checking new version ..."

        # While a spec is what is defined in the Gemfile.lock, so the current
        # gem version and all its dependency versions.
        spec = find_spec_for(dependency.name)
        puts "[#{self.class.name}][#{dependency.name}] current version is " \
             "#{spec.version}"

        dependecy_updates = build_updates_for(dependency, spec)

        if dependecy_updates != {}
          context.dependencies_hash[dependency.name] = {
            current_version: spec.version,
            updates: dependecy_updates
          }
        end
      end

      puts "[#{self.class.name}] dependencies updates: " \
           "#{context.dependencies_hash.inspect}"
    end

    private

    def sanity_checks!
      return if context.project_dependencies

      context.fail!(error: 'No project dependencies defined.')
    end

    #
    # Builds a Hash with a `:soft` key which has the next version matching the
    # dependency requirement, and a second key `:hard`, which contains the next
    # update, not matching the dependency requirement.
    #
    def build_updates_for(dependency, spec)
      updates = {}

      gem_versions = fetch_rubygems_gem_versions(dependency.name)
      gem_versions.map do |gem_version|
        next if gem_version_to_be_ignored?(gem_version['number'])

        puts "[#{self.class.name}][#{dependency.name}] Checking version " \
             "#{gem_version['number']} ..."

        remote_gem = Gem::Dependency.new(dependency.name, gem_version['number'])

        remote_version = Gem::Version.new(gem_version['number'])

        if remote_version > spec.version
          # @see Gem::Dependency#=~
          if dependency =~ remote_gem
            updates[:soft] = remote_version
            return updates # As soon as a soft update has been found, leave this.
          else
            # This will replace the previous value so that when a soft update has
            # been found, this should be the very next version.
            updates[:hard] = remote_version
          end
        end
      end

      updates
    end

    def find_spec_for(dependency_name)
      context.bundler_definition.locked_gems.specs.detect do |spec|
        spec.name == dependency_name
      end
    end

    #
    # Fetches from Rubygems.org the given gem versions ordered from the newest
    # to the oldest version.
    #
    def fetch_rubygems_gem_versions(dependency_name)
      context.gems_client.versions(dependency_name)
    rescue JSON::ParserError
      # This could happen when Rugygems fails
      puts "[#{self.class.name}] Failed to fetch #{dependency_name} versions " \
            'from Rubygems.org.'
      context.fail!(error: 'Rubygems.org response was not a valid JSON ' \
                           'message. Is it down?')
    end

    #
    # Versions to be ignored are versions which contains more than numbers and
    # dots like '1.0-rc' or '1.0.0.pre1' and so on.
    #
    def gem_version_to_be_ignored?(version)
      (version =~ /^[\d\.]+$/) == nil
    end

    def initialize_client
      context.gems_client = Gems::Client.new
    end
  end
end
